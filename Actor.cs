﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    abstract class Actor
    {
        protected Point _point;
        protected char _face;
        public Queue<Direction> _directions;
        public Direction _direction;
        public bool NeedsToReDraw;
        public char _NearSymbol;  //элемент массива, на котором он находится и следующий элемент

        public Actor(Point p, char f)
        {
            _point = p;
            _face = f;
            _directions = new Queue<Direction>();
            _NearSymbol = ' ';
            _direction = Direction.None;
            NeedsToReDraw = true;
        }
        public Actor(Matrix mas, Point p, char f)
        {
            _point = p;
            _face = f;
            _directions = new Queue<Direction>();
            _direction = Direction.None;
            NeedsToReDraw = true;
            _NearSymbol = mas.symbols[p.GetX()][p.GetY()];
        }
        public Actor(Point p, char f, Direction dir)
        {
            _point = p;
            _face = f;
            _directions = new Queue<Direction>();
            _directions.Enqueue(dir);
            _direction = Direction.None;
            _NearSymbol = ' ';
            NeedsToReDraw = true;
        }
        public Actor(Matrix mas, Point p, char f, Direction dir)
        {
            _point = p;
            _face = f;
            _directions = new Queue<Direction>();
            _directions.Enqueue(dir);
            _direction = Direction.None;
            NeedsToReDraw = true;
            _NearSymbol = mas.symbols[p.GetX()][p.GetY()];
        }
        protected void Erase()
        {
            _point.SetCursor();
            Console.Write(" ");
        }
        public void Draw()
        {
            _point.SetCursor();
            Console.Write(_face);
        }
        public virtual Direction ShowDirection()
        {
            return Direction.None;
        }
        public virtual Direction GetDirection()
        {
            return Direction.None;
        }
        public virtual void AddDirection(Direction dir)
        {
            _directions.Enqueue(dir);
        }
        protected virtual void Move(Matrix mas, Direction dir){

        }
        /*protected virtual void Move(Direction dir)
        {

        }*/
        public virtual void ReDraw(Matrix mas, Direction dir)
        {

        }
        public virtual void Fire(List<Actor> extra)
        {

        }
        /*public virtual void ReDraw(Direction dir)
        {
            
        }*/
        protected TypeOfWall FromIntToType(int t)
        {
            switch (t)
            {
                case 0:
                    return TypeOfWall.Empty;
                case 1:
                    return TypeOfWall.Forest;
                case 2:
                    return TypeOfWall.SoftWall;
                case 3:
                    return TypeOfWall.SolidWall;
                case 4:
                    return TypeOfWall.Help;
                default: return TypeOfWall.SolidWall;
            }
        }
        protected bool CanMove(Matrix mas, Point p)
        {
            if (FromIntToType(mas._mas[p.GetX()][p.GetY()]) == TypeOfWall.Empty || FromIntToType(mas._mas[p.GetX()][p.GetY()]) == TypeOfWall.Help || FromIntToType(mas._mas[p.GetX()][p.GetY()]) == TypeOfWall.Forest)
                return true;
            else
                return false;
        }
        public Point GetPoint()
        {
            return _point;
        }
        public virtual void Kill(List<Actor> actors, int ind)
        {
            actors.RemoveAt(ind);
            ind--;
        }
        protected virtual void TakeBonus(char h)
        {
            
        }
        public char GetFace()
        {
            return _face;
        }
        public virtual bool CanFire()
        {
            return true;
        }
        public static Direction RandomDirection(Matrix mas, Point p)
        {
            Random ran = new Random();
            int r = ran.Next(1, 4);
            switch (r)
            {
                    case 1:
                        if (!Matrix.IsWall(mas, p.GetX(), p.GetY() + 1))
                            return Point.PointToDirection(p, new Point(p.GetX(), p.GetY() + 1));
                        break;
                    case 2:
                        if (!Matrix.IsWall(mas, p.GetX(), p.GetY() - 1))
                            return Point.PointToDirection(p, new Point(p.GetX(), p.GetY() - 1));
                        break;
                    case 3:
                        if (!Matrix.IsWall(mas, p.GetX() + 1, p.GetY()))
                            return Point.PointToDirection(p, new Point(p.GetX() + 1, p.GetY()));
                        break;
                    case 4:
                        if (!Matrix.IsWall(mas, p.GetX() - 1, p.GetY()))
                            return Point.PointToDirection(p, new Point(p.GetX() - 1, p.GetY()));
                        break;
                    default:
                        return Direction.None;
                        //return Point.PointToDirection(p, new Point(p.GetX(), p.GetY() + 1));
            }
            return Direction.None;
        }
    }
}
