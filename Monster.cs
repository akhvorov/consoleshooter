﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    class Monster : Actor
    {
        public Monster(Matrix mas, Point p) : base(p, '☺')
        {
        }

        protected override void Move(Matrix mas, Direction dir)
        {
            _point.SetCursor();
            Console.Write(_NearSymbol);
            switch (dir)
            {
                case Direction.Up:
                case Direction.Right:
                case Direction.Down:
                case Direction.Left:
                    _direction = dir;
                    _point = _point + dir;
                    NeedsToReDraw = true;
                    _NearSymbol = mas.symbols[_point.GetX()][_point.GetY()];
                    TakeBonus(_NearSymbol);
                    break;
                case Direction.None:
                    NeedsToReDraw = false;
                    break;
            }
        }
        public override void ReDraw(Matrix mas, Direction dir)
        {
            Erase();
            if (Control.IsMoveDirection(dir))
                Move(mas, dir);
            Draw();
        }
        public override void Fire(List<Actor> extra)
        {
            Direction dir = GetDirection();
            if ((_direction == Direction.Down || _direction == Direction.Right || _direction == Direction.Left || _direction == Direction.Up)/* && CanFire()*/)
            {
                Console.SetCursorPosition(20, 7);
                Console.Write("wow!");
                NeedsToReDraw = true;
                extra.Add(new Bullet(_point + _direction, _direction));
            }
        }
        public override Direction GetDirection()
        {
            return _directions.Dequeue();
        }
        public override Direction ShowDirection()
        {
            return _directions.Peek();
        }
        public override void AddDirection(Direction dir)
        {
            _directions.Enqueue(dir);
        }
        public override void Kill(List<Actor> actors, int ind)
        {
            actors[ind].GetPoint().SetCursor();
            Console.WriteLine(" ");
            actors.RemoveAt(ind);
            ind--;
        }
    }
}
