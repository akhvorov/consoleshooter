﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
//using System.Windows.Forms;

namespace PackmanS2
{
    class Matrix
    {
        public int[][] _mas;
        public char[][] symbols;
        public int _hight;
        public int _width;

        public Matrix(string FileName)
        {
            _hight = LengthLabN(FileName);
            _width = LengthLabM(_hight, FileName);
            _mas = new int[_hight][];
            for (int i = 0; i < _hight; i++)
                _mas[i] = new int[_width];
            _mas = InputLabirint(_hight, _width, FileName);
            symbols = new char[_hight][];
            for (int i = 0; i < _hight; i++)
                symbols[i] = new char[_width];
            symbols = MakeLabNice(_mas, _hight, _width);

            // добавляет жизни и патроны дополнительные
            //mas = AddToLab(mas, _hight, _width/*, O, K*/);
        }

        /*
         * 1 - пропуск, свободное место
         * 2 - стена непробиваемая
         * 3 - 
         */

        static int FromCharToInt(char h)
        {
            return (int)(h - '0');
        }
        static TypeOfWall FromIntToType(int t)
        {
            switch (t)
            {
                case 0:
                    return TypeOfWall.Empty;
                case 1:
                    return TypeOfWall.Forest;
                case 2:
                    return TypeOfWall.SoftWall;
                case 3:
                    return TypeOfWall.SolidWall;
                case 4:
                    return TypeOfWall.Help;
                default: return TypeOfWall.SolidWall;
            }
        }
        public static TypeOfWall FromIntToType(Matrix mas, Point p)
        {
            int t = mas._mas[p.GetX()][p.GetY()];
            switch (t)
            {
                case 0:
                    return TypeOfWall.Empty;
                case 1:
                    return TypeOfWall.Forest;
                case 2:
                    return TypeOfWall.SoftWall;
                case 3:
                    return TypeOfWall.SolidWall;
                case 4:
                    return TypeOfWall.Help;
                default: return TypeOfWall.SolidWall;
            }
        }
        public void Print()
        {
            for (int i = 0; i < _hight; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    switch (_mas[i][j])
                    {
                        case 0:
                        case 1:
                        case 3:
                        case 4: Console.ForegroundColor = ConsoleColor.White; break;
                        case 2: Console.ForegroundColor = ConsoleColor.Gray; break;
                    }
                    Console.Write(symbols[i][j]);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine();
            }
        }
        static int LengthLabN(string Fname)
        {
            StreamReader stream = new StreamReader(Fname);
            string s;
            int n = 0;
            while ((s = stream.ReadLine()) != null)
                n++;
            stream.Close();
            return n;
        }
        static int LengthLabM(int n, string Fname)
        {
            StreamReader stream = new StreamReader(Fname);
            string s;
            int m = 0;
            while ((s = stream.ReadLine()) != null)
                if (m < s.Length)
                    m = s.Length;
            stream.Close();
            return m;
        }
        private int[][] InputLabirint(int n, int m, string Fname)
        {
            int[][] mas = new int[n][];
            for (int i = 0; i < n; i++)
                mas[i] = new int[m];
            StreamReader stream = new StreamReader(Fname);
            string s;
            int pl = 0;
            while ((s = stream.ReadLine()) != null)
            {
                for (int j = 0; j < s.Length; j++)
                    mas[pl][j] = FromCharToInt(s[j]);
                for (int i = s.Length; i < m; i++)
                    mas[pl][i] = 0;
                pl++;
            }
            stream.Close();
            return mas;
        }
        private char WhatSymbol(int[][] m)
        {
            char[][] symb = { new char[] { '╔', '╦', '╗' }, new char[] { '╠', '╬', '╣' }, new char[] { '╚', '╩', '╝' } };
            char vertical = '║';
            char horizontal = '═';
            int up = 0, down = 0, right = 0, left = 0;
            if (m[1][1] == 0)
                return ' ';
            if (m[1][1] == 1)
                return '▒';
            if (m[1][1] == 4)
                return 'O';
            if (m[0][1] == 2 || m[0][1] == 3)
                up++;
            if (m[2][1] == 2 || m[2][1] == 3)
                down++;
            if (m[1][0] == 2 || m[1][0] == 3)
                left++;
            if (m[1][2] == 2 || m[1][2] == 3)
                right++;
            if ((left == 1 || right == 1) && m[0][1] != 2 & m[2][1] != 2 && m[0][1] != 3 & m[2][1] != 3)
                return horizontal;
            if ((up == 1 || down == 1) && m[1][0] != 2 & m[1][2] != 2 && m[1][0] != 3 & m[1][2] != 3)
                return vertical;
            return symb[1 + up - down][1 - right + left];


            /*
            if ((m[0][0] != ' ') && (m[0][1] != ' ') && (m[0][2] != ' ') && (m[1][0] != ' ') && (m[1][2] != ' ') && (m[2][0] != ' ') && (m[2][1] != ' ') && (m[2][2] != ' '))
            {
                    return ' ';  //' '
            }
            if ((m[0][1] == ' ') && (m[1][0] == ' ') && (m[1][2] == ' ') && (m[2][1] == ' '))
            {
                return '▀';  //когда совсем один и похожие варианты
            }
            if ((m[0][1] == ' ') && (m[2][1] == ' ') && ((m[1][0] != ' ') || (m[1][2] != ' ')))
            {
                return '═';  //═
            }
            if ((m[1][0] == ' ') && (m[1][2] == ' ') && ((m[2][1] != ' ') || (m[0][1] != ' ')))
            {
                return '║';  //║
            }
            if ((m[0][1] == ' ') && (m[1][0] == ' ') && (m[1][2] != ' ') && (m[2][1] != ' '))
            {
                return '╔';  //╔
            }
            if ((m[0][1] != ' ') && (m[1][0] != ' ') && (m[1][2] == ' ') && (m[2][1] == ' '))
            {
                return '╝';  //╝
            }
            if ((m[0][1] == ' ') && (m[1][0] != ' ') && (m[1][2] == ' ') && (m[2][1] != ' '))
            {
                return '╗';  //╗
            }
            if ((m[0][1] != ' ') && (m[1][0] == ' ') && (m[1][2] != ' ') && (m[2][1] == ' '))
            {
                return '╚';  //╚
            }
            if ((m[0][1] != ' ') && (m[1][0] == ' ') && (m[1][2] != ' ') && (m[2][1] != ' '))
            {
                return '╠';  //╠
            }
            if ((m[0][1] != ' ') && (m[1][0] != ' ') && (m[1][2] != ' ') && (m[2][1] == ' '))
            {
                return '╩';  //╩
            }
            if ((m[0][1] == ' ') && (m[1][0] != ' ') && (m[1][2] != ' ') && (m[2][1] != ' '))
            {
                return '╦';  //╦
            }
            if ((m[0][1] != ' ') && (m[1][0] != ' ') && (m[1][2] == ' ') && (m[2][1] != ' '))
            {
                return '╣';  //╣
            }
            if ((m[0][1] != ' ') && (m[1][0] != ' ') && (m[1][2] != ' ') && (m[2][1] != ' '))
            {
                return '╬';  //╬
            }
            return ' ';*/
            
        }
        private char[][] MakeLabNice(int[][] mas, int n, int m)
        {
            //char[] symb;
            char[][] symb = new char[n][];
            for (int i = 0; i < n; i++)
                symb[i] = new char[m];
            int[][] a = new int[3][];
            for (int h = 0; h < 3; h++)
                a[h] = new int[3];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    for (int p = i - 1; p <= i + 1; p++)
                        for (int k = j - 1; k <= j + 1; k++)
                        {
                            if ((p >= 0) && (p < n) && (k >= 0) && (k < m))
                                a[p - i + 1][k - j + 1] = mas[p][k];
                            else
                                a[p - i + 1][k - j + 1] = 0;
                        }
                    symb[i][j] = WhatSymbol(a);
                }
            return symb;
        }
        private Point SetCoord(Matrix mas, Random r)
        {
            int x, y;
            x = r.Next(0, _hight);
            y = r.Next(0, _width);
            while (_mas[x][y] != ' ')
            {
                x = r.Next(0, _hight);
                y = r.Next(0, _width);
            }
            Point p = new Point(x, y);
            return p;
        }
        public void Set(Point p, int h){
            _mas[p.GetX()][p.GetY()] = h;
        }
        public void CrashWall(Point p)
        {
            _mas[p.GetX()][p.GetY()] = 0;
            symbols[p.GetX()][p.GetY()] = ' ';
        }
        public static bool IsWall(Matrix mas, Point p)
        {
            if (mas._mas[p.GetX()][p.GetY()] == 2 || mas._mas[p.GetX()][p.GetY()] == 3)
                return true;
            else
                return false;
        }
        public static bool IsWall(Matrix mas, int x, int y)
        {
            if (mas._mas[x][y] == 2 || mas._mas[x][y] == 3)
                return true;
            else
                return false;
        }
    }
}
