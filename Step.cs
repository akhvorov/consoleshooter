﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    class Step
    {
        private int _x;
        private int _y;
        //private Point _p;
        private int length;
        public Step()
        {
            _x = 0;
            _y = 0;
            length = 0;
        }
        public Step(int a, int b, int l)
        {
            _x = a;
            _y = b;
            length = l;
        }
        public Step(Point p, int l)
        {
            _x = p.GetX();
            _y = p.GetY();
            length = l;
        }
        public int GetX()
        {
            return _x;
        }
        public int GetY()
        {
            return _y;
        }
        public Point GetPoint()
        {
            Point p = new Point(_x, _y);
            return p;
        }
        public int GetL()
        {
            return length;
        }
    }
}
