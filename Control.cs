﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    class Control
    {
        public static bool IsShootDirection(Direction dir)
        {
            if (dir == Direction.Shoot)
                return true;
            else
                return false;
        }
        public static bool IsMoveDirection(Direction dir)
        {
            /*if (key == ConsoleKey.UpArrow ||
                key == ConsoleKey.RightArrow ||
                key == ConsoleKey.DownArrow ||
                key == ConsoleKey.LeftArrow ||
                key == ConsoleKey.W ||
                key == ConsoleKey.D ||
                key == ConsoleKey.S ||
                key == ConsoleKey.A)
            {
                    return true;
            }
            else
            {
                return false;
            }*/
            /*if (dir == Direction.Up || 
                dir == Direction.Right || 
                dir == Direction.Down ||
                dir == Direction.Left)
            {
                return true;
            }
            else{
                return false;
            }*/
            return (dir == Direction.Up ||
                dir == Direction.Right ||
                dir == Direction.Down ||
                dir == Direction.Left);
        }
        public static Direction ConsoleKeyToDirection(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.UpArrow:
                case ConsoleKey.W:
                    return Direction.Up;
                case ConsoleKey.RightArrow:
                case ConsoleKey.D:
                    return Direction.Right;
                case ConsoleKey.DownArrow:
                case ConsoleKey.S:
                    return Direction.Down;
                case ConsoleKey.LeftArrow:
                case ConsoleKey.A:
                    return Direction.Left;
                case ConsoleKey.Spacebar:
                    return Direction.Shoot;
                default: return Direction.None;
            }
        }
    }
}
