﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PackmanS2
{
    class Scene
    {
        private ConsoleKeyInfo cki;


        public Scene(Matrix mas, List<Actor> actors){
            mas.Print();
            cki = new ConsoleKeyInfo();
            Console.CursorVisible = false;
        }

        public void Move(Matrix mas, List<Actor> actors)
        {
            Player player = (Player)actors[0];
            List<Actor> extra = new List<Actor>();
            int re = 5;
            int im = 5;
            while ((cki.Key != ConsoleKey.Escape) /*&& (player.GetHP() > 0)*/)
            {
                while (!Console.KeyAvailable)
                {
                    foreach (Actor a in actors)
                    {
                        if (a._directions.Count != 0)
                        {
                            if (a.ShowDirection() != Direction.Shoot && a.NeedsToReDraw)
                            {
                                a.ReDraw(mas, a.GetDirection());
                                re++;
                            }
                            else if (a.CanFire())
                                a.Fire(extra);
                            else{
                                a.GetDirection();
                                a.ReDraw(mas, Direction.None);
                            }
                        }
                        else
                            a.ReDraw(mas, Direction.None);
                    }
                    foreach (Actor e in extra)
                        actors.Add(e);
                    Console.SetCursorPosition(20, 1);
                    Console.Write("{0} - кол-во актеров", actors.Count);
                    Thread.Sleep(100);
                    extra.Clear();
                    if (actors.Count > 1)
                        Dead(actors, mas);
                }
                cki = Console.ReadKey(true);
                foreach (Actor a in actors)
                {
                    if (a != player)
                        a.AddDirection(MonstersAI.MonsterDirection(actors, mas, a, player));
                    else
                        a.AddDirection(Control.ConsoleKeyToDirection(cki.Key));
                }
            }
        }
        private void Dead(List<Actor> actors, Matrix mas)
        {
            TypeOfWall w = TypeOfWall.Empty;
            for (int i = 0; i < actors.Count; i++)
            {
                w = Matrix.FromIntToType(mas, actors[i].GetPoint() + actors[i]._direction);
                if (actors[i].GetFace() == 'o' && ((w == TypeOfWall.SoftWall) || (w == TypeOfWall.SoftWall)))
                {
                    if (w == TypeOfWall.SoftWall)
                        mas.CrashWall(actors[i].GetPoint() + actors[i]._direction);
                    actors[i].Kill(actors, i);
                    i--;
                }
            }
            for (int num = 0; num < actors.Count; num++)
            {
                for (int i = num; i < actors.Count; i++)
                {
                    if (((actors[num].GetPoint() == actors[i].GetPoint()) && AreKill(actors[num].GetFace(), actors[i].GetFace())))
                    {
                        actors[i].Kill(actors, i);
                        actors[num].Kill(actors, num);
                        //i--;
                    }
                }
            }
        }

        private bool AreKill(char a, char b)
        {
            if ((a == '☺' && b == '☻') || (a == '☻' && b == 'o') || (a == '☻' && b == '☺') || (a == 'o' && b == '☻') || (a == '☺' && b == 'o') || (a == 'o' && b == '☺'))
                return true;
            else
                return false;
        }
    }
}
