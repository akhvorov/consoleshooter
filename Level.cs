﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    class Level
    {
        private int _numOfMonsters;
        private string FileName;


        public Level(int lev)
        {
            switch (lev)
            {
                case 1: 
                    _numOfMonsters = 3;
                    FileName = @"C:\Users\Саша\Documents\Visual Studio 2013\Projects\PackmanS2\PackmanS2\Labirint.txt";
                    break;
                case 2: _numOfMonsters = 4; break;
            }
            Matrix mas = new Matrix(FileName);
            Random r = new Random();
            //Point p = new Point(2, 5);
            //Player player = new Player(mas, p);
            List<Actor> actors = new List<Actor>();
            actors.Add(new Player(mas, SetCoord(mas, r)));
            for (int i = 0; i < _numOfMonsters; i++)
                actors.Add(new Monster(mas, SetCoord(mas, r)));
            Scene scene = new Scene(mas, actors);
            scene.Move(mas, actors);

        }
        public static Point SetCoord(Matrix mas, Random r)
        {
            int x, y;
            x = r.Next(0, mas._hight);
            y = r.Next(0, mas._width);
            while (mas._mas[x][y] != 0)
            {
                x = r.Next(0, mas._hight);
                y = r.Next(0, mas._width);
            }
            Point p = new Point(x, y);
            return p;
        }
    }
}
