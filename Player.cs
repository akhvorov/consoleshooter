﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    class Player : Actor
    {
        private int _HP;
        private int _bullets;

        public Player(Point p)
            : base(p, '☻')
        {
            _HP = 3;
            _bullets = 3;
        }
        public Player(Matrix mas, Point p)
            : base(mas, p, '☻')
        {
            _HP = 3;
            _bullets = 10;
        }
        public void SetHP (int h){
            _HP = h;
        }
        public int GetHP()
        {
            return _HP;
        }
        protected override void Move(Matrix mas, Direction dir)
        {
            _point.SetCursor();
            Console.Write(_NearSymbol);
            switch (dir)
            {
                case Direction.Up:
                case Direction.Right:
                case Direction.Down:
                case Direction.Left:
                    _direction = dir;
                    if (CanMove(mas, (_point + dir))){
                        _point = _point + dir;
                        NeedsToReDraw = true;
                        _NearSymbol = mas.symbols[_point.GetX()][_point.GetY()];
                        TakeBonus(_NearSymbol);
                    }
                    else{
                        //NeedsToReDraw = false;
                        NeedsToReDraw = true;
                    }
                    break;
                case Direction.None:
                    //NeedsToReDraw = false;
                    NeedsToReDraw = true;
                    break;
                //default: NeedsToReDraw = false;
            }
        }

        public override void ReDraw(Matrix mas, Direction dir)
        {
            Erase();
            if (Control.IsMoveDirection(dir))
                Move(mas, dir);
            Draw();
        }
        public override void Fire(List<Actor> extra)
        {
            Direction dir = GetDirection();
            if ((_direction == Direction.Down || _direction == Direction.Right || _direction == Direction.Left || _direction == Direction.Up)/* && CanFire()*/)
            {
                Console.SetCursorPosition(20, 2);
                Console.Write("{0}", _bullets);
                NeedsToReDraw = true;
                _bullets--;
                Console.SetCursorPosition(20, 2);
                Console.Write("{0} - осталось пуль", _bullets);
                extra.Add(new Bullet(_point + _direction, _direction));
            }
        }
        public override bool CanFire()
        {
            if (_bullets > 0)
                return true;
            else
                return false;
        }
        public override Direction GetDirection()
        {
            return _directions.Dequeue();
        }
        public override Direction ShowDirection()
        {
            return _directions.Peek();
        }
        public override void AddDirection(Direction dir)
        {
            _directions.Enqueue(dir);
        }
        public override void Kill(List<Actor> actors, int ind)
        {
            _HP--;
            actors[ind].GetPoint().SetCursor();
            actors[ind].Draw();
            Console.SetCursorPosition(20, 0);
            Console.WriteLine("{0}", _HP);
        }
        protected override void TakeBonus(char h)
        {
            switch (h)
            {
                case 'O': _bullets++; break;
                case 'K': _HP++; break;
            }
        }
    }
}
