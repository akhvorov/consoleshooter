﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    class Bullet : Actor
    {
        public Bullet(Point p, Direction dir) : base(p, 'o', dir)
        {
            _direction = dir;
        }
        protected override void Move(Matrix mas, Direction dir)
        {
            //base.Move(mas, dir);
            _point.SetCursor();
            Console.Write(_NearSymbol);
            if (CanMove(mas, (_point + dir))){
                 _point = _point + dir;
                 NeedsToReDraw = true;
                 _NearSymbol = mas.symbols[_point.GetX()][_point.GetY()];
            }
        }
        public override void ReDraw(Matrix mas, Direction dir)
        {
            Erase();
            Move(mas, dir);
            Draw();
        }
        public override Direction GetDirection()
        {
            return ShowDirection();
        }
        public override Direction ShowDirection()
        {
            return _direction/*_directions.Peek()*/;
        }
        public override void AddDirection(Direction dir)
        {
        }
        public override void Kill(List<Actor> actors, int ind)
        {
            actors[ind].GetPoint().SetCursor();
            Console.WriteLine(" ");
            actors.RemoveAt(ind);
            ind--;
        }
    }
}
