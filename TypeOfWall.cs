﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    enum TypeOfWall
    {
        Empty = 0,
        Forest = 1,
        SoftWall = 2,
        SolidWall = 3,
        Help = 4
    }
}
