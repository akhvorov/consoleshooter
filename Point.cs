﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Math;

namespace PackmanS2
{
    class Point
    {
        private int _x;
        private int _y;

        public Point()
        {
            _x = 0;
            _y = 0;
        }
        public Point(int x, int y)
        {
            _x = x;
            _y = y;
        }
        public static Point operator+(Point r, Direction dir)
        {
            Point p = new Point(r._x, r._y);
            switch (dir)
            {
                case Direction.Up:
                    p._x--;
                    break;
                case Direction.Right:
                    p._y++;
                    break;
                case Direction.Down:
                    p._x++;
                    break;
                case Direction.Left:
                    p._y--;
                    break;
                default: break;
            }
            return p;
        }
        public void SetCursor()
        {
            Console.SetCursorPosition(_y, _x);
        }
        public int GetX(){
            return _x;
        }
        public int GetY()
        {
            return _y;
        }
        public static int Distance(Point t, Point p)
        {
            int r = abs(t._x - p._x) + abs(t._y - p._y);
            return r;
        }
        public static int abs(int a)
        {
            if (a >= 0)
                return a;
            else
                return -a;
        }
        public static bool operator ==(Point a, Point b)
        {
            if (a._x == b._x && a._y == b._y)
                return true;
            else
                return false;
        }
        public static bool operator !=(Point a, Point b)
        {
            if (a._x == b._x && a._y == b._y)
                return false;
            else
                return true;
        }
        public static Direction PointToDirection(Point S, Point F)
        {
            if (Distance(S, F) == 1)
            {
                if (S._x == F._x)
                {
                    if (S._y < F._y)
                        return Direction.Right;
                    else
                        return Direction.Left;
                }
                else
                {
                    if (S._x < F._x)
                        return Direction.Down;
                    else
                        return Direction.Up;
                }
            }
            else
                return Direction.None;
        }
    }
}
