﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackmanS2
{
    /// <summary>
    /// Здесь все очень плохо, не знаю, почему тогда я писал так...
    /// </summary>
    class MonstersAI
    {
        public static Direction MonsterDirection(List<Actor> actors, Matrix mas, Actor M, Actor P)
        {
            switch (DirectionForBullet(actors, mas, M, P))
            {
                case Direction.Up:
                case Direction.Down:
                case Direction.Right:
                case Direction.Left:
                    Console.SetCursorPosition(20, 6);
                    Console.WriteLine("###");
                    return Direction.Shoot;
                default: break;
            }
            int[][] s = new int[mas._hight][];
            for (int i = 0; i < mas._hight; i++)
                s[i] = new int[mas._width];
            for (int i = 0; i < mas._hight; i++)
                for (int j = 0; j < mas._width; j++)
                {
                    if (mas._mas[i][j] == 0 || mas._mas[i][j] == 1 || mas._mas[i][j] == 4)
                        s[i][j] = 0;  //проходимое
                    else if (mas._mas[i][j] == 2 || mas._mas[i][j] == 3)
                        s[i][j] = -2; //стена
                    else
                        s[i][j] = -1; //стена
                }
            Point p = new Point(P.GetPoint().GetX(), P.GetPoint().GetY());
            Point m = new Point(M.GetPoint().GetX(), M.GetPoint().GetY());
            if (Point.Distance(m, p) == 1)
                return Point.PointToDirection(M.GetPoint(), P.GetPoint());
            Point g = new Point();
            int x, y;
            int l = 0;
            Queue<Step> w = new Queue<Step>();
            Step t = new Step(p, l);
            bool bol = false;
            s[p.GetX()][p.GetY()] = 0;
            while (!(t.GetPoint() == m))
            {
                l = t.GetL() + 1;
                x = t.GetX();
                y = t.GetY();
                g = t.GetPoint();
                if ((s[x - 1][y] == 0)/* || (s[x - 1][y] == -2)*/)
                {
                    Step a1 = new Step((x - 1), y, l);
                    s[x - 1][y] = a1.GetL();
                    w.Enqueue(a1);
                    bol = true;
                }
                if ((s[x + 1][y] == 0)/* || (s[x + 1][y] == -2)*/)
                {
                    Step a2 = new Step((x + 1), y, l);
                    s[x + 1][y] = a2.GetL();
                    w.Enqueue(a2);
                    bol = true;
                }
                if ((s[x][y - 1] == 0)/* || (s[x][y - 1] == -2)*/)
                {
                    Step a3 = new Step(x, (y - 1), l);
                    s[x][y - 1] = a3.GetL();
                    w.Enqueue(a3);
                    bol = true;
                }
                if ((s[x][y + 1] == 0)/* || (s[x][y + 1] == -2)*/)
                {
                    Step a4 = new Step(x, (y + 1), l);
                    s[x][y + 1] = a4.GetL();
                    w.Enqueue(a4);
                    bol = true;
                }
                if (Point.Distance(g, m) > 10) //если слишком далеко, чтоб не шел
                    Actor.RandomDirection(mas, g);
                if (Point.Distance(g, m) == 1)
                    return Point.PointToDirection(m, g);
                t = w.Dequeue();
            }
            return Direction.None;
        }
        private static Direction DirectionForBullet(List<Actor> actors, Matrix mas, Actor M, Actor P)
        {
            Point m = M.GetPoint();
            Point p = P.GetPoint();
            bool b = true;
            Direction dir = M._direction;
            switch (dir)
            {
                case Direction.Up:
                case Direction.Down:
                    if (m.GetY() == p.GetY())
                    {
                        while ((!Matrix.IsWall(mas, m)) && (m != p))
                        {
                            foreach (Actor a in actors)
                                if ((m == a.GetPoint()) && (a.GetPoint() == p/*a != actors[0]*/))
                                {
                                    Console.SetCursorPosition(20, 7);
                                    Console.WriteLine("??? -");
                                    b = false;
                                }
                            m = m + dir;
                        }
                        if ((m == p) && b)
                            return dir;
                    }
                    b = true;
                    break;
                case Direction.Right:
                case Direction.Left:
                    if (m.GetX() == p.GetX())
                    {
                        while ((!Matrix.IsWall(mas, m)) && (m != p))
                        {
                            foreach (Actor a in actors)
                                if ((m == a.GetPoint()) && (a != actors[0]))
                                    b = false;
                            m = m + dir;
                        }
                        if ((m == p) && b)
                            return dir;
                    }
                    break;
                default: return Direction.None;
            }
            return Direction.None;
        }
    }
}
